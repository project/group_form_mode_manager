<?php

namespace Drupal\group_form_mode_manager\Plugin\EntityRoutingMap;

use Drupal\form_mode_manager\EntityRoutingMapBase;

/**
 * Class Group.
 *
 * @EntityRoutingMap(
 *   id = "group",
 *   label = @Translation("Group Routes properties"),
 *   targetEntityType = "group",
 *   defaultFormClass = "add",
 *   editFormClass = "edit",
 *   operations = {
 *     "add_form" = "entity.group.add_form",
 *     "edit_form" = "entity.group.edit_form",
 *     "add_page" = "group.add_page"
 *   }
 * )
 */
class Group extends EntityRoutingMapBase {

}
