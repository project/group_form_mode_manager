<?php

namespace Drupal\group_form_mode_manager\Plugin\EntityRoutingMap;

use Drupal\form_mode_manager\EntityRoutingMapBase;

/**
 * Class GroupContent.
 *
 * @EntityRoutingMap(
 *   id = "group_content",
 *   label = @Translation("Group Content Routes properties"),
 *   targetEntityType = "group_content",
 *   defaultFormClass = "add",
 *   editFormClass = "edit",
 *   operations = {
 *     "group-join" = "entity.group.join",
 *     "group-leave" = "entity.group.leave",
 *     "add_form" = "entity.group_content.add_form",
 *     "edit_form" = "entity.group_content.edit_form",
 *     "delete_form" = "entity.group_content.delete_form",
 *     "group_node_add_page" = "entity.group_content.group_node_add_page",
 *     "group_node_relate_page" = "entity.group_content.group_node_relate_page",
 *     "create_form" = "entity.group_content.create_form",
 *     "create_page" = "entity.group_content.create_page"
 *   }
 * )
 */
class GroupContent extends EntityRoutingMapBase {

}
