<?php

namespace Drupal\group_form_mode_manager\Controller;

use Drupal\form_mode_manager\Controller\FormModeManagerEntityController;
use Drupal\group_form_mode_manager\GroupContentEntityFormModes;

/**
 * Group Controller for entity using form mode manager routing.
 *
 * This controller are very transverse and use an Abstract Factory to build,
 * objects compatible with all ContentEntities. This controller are linked by,
 * Abstract Factory by EntityFormModeManagerInterface each methods in that,
 * interface are called by routing.
 */
class GroupContentFormModeManagerEntityController extends FormModeManagerEntityController {

  /**
   * Get the correct controller object Factory depending kind of entity.
   *
   * @param string $entity_type_id
   *   The name of entity type.
   *
   * @return \Drupal\form_mode_manager\EntityFormModeManagerInterface
   *   An instance of correct controller object.
   */
  public function getEntityControllerObject($entity_type_id) {
    return new GroupContentEntityFormModes(
      $this->renderer,
      $this->account,
      $this->formModeManager,
      $this->entityFormBuilder,
      $this->entityRoutingMap,
      $this->formBuilder,
      $this->entityTypeManager
    );
  }

}
