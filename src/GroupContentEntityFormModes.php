<?php

namespace Drupal\group_form_mode_manager;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\form_mode_manager\ComplexEntityFormModes;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupContent;

/**
 * Route controller factory specific for Group Content entities.
 *
 * This Factory are herited from "ComplexEntityFormModes" because this entity,
 * implement some specific things proper to Group Content
 */
class GroupContentEntityFormModes extends ComplexEntityFormModes {

  /**
   * {@inheritdoc}
   */
  public function getEntityFromRouteMatch(RouteMatchInterface $route_match) {
    /** @var \Drupal\group\Entity\Group $group */
    $group = Group::load($route_match->getParameter('group'));

    if (empty($entity)) {
      $plugin = $group->getGroupType()->getContentPlugin('group_membership');

      // Pre-populate a group membership with the current user.
      $entity = GroupContent::create([
        'type' => $plugin->getContentTypeConfigId(),
        'gid' => $group->id(),
        'entity_id' => \Drupal::currentUser()->id(),
      ]);
    }

    return $entity;
  }

  /**
   * {@inheritdoc}
   */
  public function pageTitle(RouteMatchInterface $route_match, $operation) {
    /** @var \Drupal\group\Entity\Group $group */
    $group = Group::load($route_match->getParameter('group'));
    return $this->t('Join group %label', ['%label' => $group->label()]);
  }

}
