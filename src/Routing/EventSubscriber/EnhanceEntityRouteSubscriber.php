<?php

namespace Drupal\group_form_mode_manager\Routing\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\form_mode_manager\Routing\EventSubscriber\EnhanceEntityRouteSubscriber as BaseEntitySubscriber;
use Drupal\form_mode_manager\FormModeManagerInterface;
use Drupal\form_mode_manager\EntityRoutingMapManager;
use Symfony\Component\Routing\RouteCollection;

/**
 * Listens to the dynamic route event and enhance existing routes.
 *
 * To provide a more flexible system as possible we need to add some,
 * parameters dynamically onto each entities using form modes to,
 * applies our logic. In the current case Form Mode Manager provide,
 * ability to add more access granularity to `Default` entity routes.
 */
class EnhanceEntityRouteSubscriber extends BaseEntitySubscriber {

  /**
   * Add form mode manager requirements to add more access granularity.
   *
   * {@inheritdoc}
   */
  protected function alterRoutes(RouteCollection $collection) {
    $entity_type_id = 'group_content';
    $this->routeCollection = $collection;
    $this->entityDefinition = $this->entityTypeManager->getDefinition($entity_type_id);
    $this->entityRoutingDefinition = $this->entityRoutingMap->createInstance($entity_type_id, ['entityTypeId' => $entity_type_id]);
    $this->enhanceDefaultEntityRoute('group-join');
  }

}
